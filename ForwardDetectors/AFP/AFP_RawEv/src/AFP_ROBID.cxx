/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include"AFP_RawEv/AFP_ROBID.h"

const uint32_t AFP_ROBID::sideA = 0x00850001;
const uint32_t AFP_ROBID::sideC = 0x00850002;

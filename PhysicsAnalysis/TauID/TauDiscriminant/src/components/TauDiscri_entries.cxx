#include "TauDiscriminant/TauJetBDT.h"
#include "TauDiscriminant/TauEleBDT.h"
#include "TauDiscriminant/TauMuonVeto.h"
#include "TauDiscriminant/TauScoreFlatteningTool.h"

DECLARE_COMPONENT( TauJetBDT )
DECLARE_COMPONENT( TauEleBDT )
DECLARE_COMPONENT( TauMuonVeto )
DECLARE_COMPONENT( TauScoreFlatteningTool )

